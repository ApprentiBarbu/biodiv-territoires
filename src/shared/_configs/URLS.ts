export const URLS = {
  home: () => "/",
  aboutUs: () => "/a-propos",
  contact: () => "/contact",
  territories: (territorySlug: string) => `/territoires/${territorySlug}`,
};
