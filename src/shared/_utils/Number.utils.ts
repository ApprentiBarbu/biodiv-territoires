export const numberUtils = {

  formatNumber(value?: number) {
    if (value === undefined) return undefined;
    let stringValue = value.toString();
    let formattedValue = "";
    for (let i = 1; i <= stringValue.length; i++) {
      formattedValue = stringValue[stringValue.length - i] + formattedValue;
      if (i % 3 === 0 && i !== stringValue.length) formattedValue = " " + formattedValue;
    }
    return formattedValue;
  },
};
