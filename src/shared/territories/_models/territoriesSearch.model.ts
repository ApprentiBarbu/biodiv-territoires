import { Territory } from "./territories.model";

export type TerritorySearchResult = Territory

export type TerritoriesSearchResult = {
  count: number;
  datas: TerritorySearchResult[];
}