export enum TerritoriesCode {
  COMMUNE = "COM",
}

export enum TerritoriesType {
  COMMUNE = "Communes",
}

export type Territory = {
  id: number;
  area_code: string;
  area_name: string;
  type_desc: string;
  type_name: TerritoriesType;
  type_code: TerritoriesCode;
}
