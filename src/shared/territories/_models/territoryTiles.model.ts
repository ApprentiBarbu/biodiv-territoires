import { FeatureCollection, Geometry } from "geojson";

export type TerritoryTileProperties = {
  area_code: string;
  area_name: string;
  count_dataset: number;
  count_date: number;
  count_observer: number;
  count_occtax: number;
  count_taxa: number;
  count_threatened: number;
  id_area: number;
  last_obs: string;
  type_code: "M0.5";
};

export type TerritoryTilesResult = FeatureCollection<Geometry, TerritoryTileProperties>;