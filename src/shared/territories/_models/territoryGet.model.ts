import { Territory } from "./territories.model";

export type TerritoryGetResult = Territory | undefined;
