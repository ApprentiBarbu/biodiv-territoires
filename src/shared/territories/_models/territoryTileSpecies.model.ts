export type TerritoryTileSpecie = {
  id: number;
  "cd_ref": number;
  "count_dataset": number;
  "count_date": number;
  "count_observer": number;
  "count_occtax": number;
  "group2_inpn": string;
  "last_year": number;
  "lb_nom": string;
  "nom_vern": string;
  threatened: boolean;
}

export type TerritoryTileSpeciesResult = {
  count: number;
  data: TerritoryTileSpecie[]
}
