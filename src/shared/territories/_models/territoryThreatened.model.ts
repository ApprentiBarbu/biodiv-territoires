export type TerritoryThreatenedResult = {
  labels: string[];
  surrounding: {
    not_threatened: number[];
    threatened: number[];
  };
  territory: {
    not_threatened: number[];
    threatened: number[];
  };
};
