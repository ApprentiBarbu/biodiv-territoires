export type TerritoryStatsResult = {
  observations: number;
  species: number;
  threatenedSpecies: number;
  observers: number;
  observedDates: number;
  lastObservation: number;
};
