export type TerritoriesCategoryConf = {
  id: number;
  max: number;
  min: number;
  ntile: number;
  type: string;
};

export type TerritoriesCategoriesConf = TerritoriesCategoryConf[];

export type TerritoriesConfResult = TerritoriesCategoriesConf;
