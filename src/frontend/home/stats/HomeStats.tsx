import React from "react";
import { HomeStatsBlock } from "./HomeStatsBlock";
import { FaKiwiBird } from "react-icons/fa";
import { RiUserFill } from "react-icons/ri";
import { ImBinoculars } from "react-icons/im";

export function HomeStats() {
  return (
    <div className="flex flex-row flex-wrap -ml-6">
      <HomeStatsBlock value="21 098 793" label="observations" icon={<ImBinoculars size={17}/>}/>
      <HomeStatsBlock value="4 812" label="taxons" icon={<FaKiwiBird size={20}/>}/>
      <HomeStatsBlock value="19 912" label="observateurs" icon={<RiUserFill size={21}/>}/>
    </div>
  );
}
