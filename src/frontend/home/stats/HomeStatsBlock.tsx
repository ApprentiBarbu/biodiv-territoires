import { motion } from "framer-motion";
import React, { ReactNode } from "react";

type Props = {
  icon?: ReactNode;
  value: string;
  label: string;
};

export function HomeStatsBlock(props: Props) {
  return (
    <div className="pl-6 border-white border-l mx-6 pb-6 border-opacity-50 mt-2 overflow-hidden">
      <motion.div
        className="text-4xl font-bold mb-1"
        initial={{ marginLeft: -400 }}
        animate={{ marginLeft: 0 }}
        transition={{ delay: 0.5, duration: 1 }}
      >
        {props.value}
      </motion.div>
      <div className="text-xl flex flex-row items-center">
        {props.icon ? <div className="mr-2">{props.icon}</div> : null}
        {props.label}
      </div>
    </div>
  );
}
