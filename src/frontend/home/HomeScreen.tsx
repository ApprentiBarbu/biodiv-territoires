import React from "react";
import styles from "./_css/home.module.css";
import clsx from "clsx";
import { HomeStats } from "./stats/HomeStats";
import { TerritoriesSearchField } from "../territories/search/TerritoriesSearchField";
import { Parteners } from "./parteners/Parteners";

export function HomeScreen() {
  return (
    <div>
      <main className={clsx("flex flex-col", styles.content)}>
        <div className="flex-1 flex flex-col justify-center">
          <h1 className={clsx("text-4xl font-bold mb-4", styles.title)}>
            Une plateforme de porter à connaissance de la biodiversité des territoires
          </h1>
          <TerritoriesSearchField/>
        </div>
        <div className="flex flex-row items-end flex-wrap justify-between">
          <HomeStats/>
        </div>
      </main>
      <Parteners/>
    </div>
  );
}
