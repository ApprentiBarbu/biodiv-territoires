import React from "react";
import Image from "next/image";

export function Parteners() {
  return (
    <div className="flex flex-col rounded-sm py-3 px-5 mt-2 absolute top-2 right-4">
      <div className="mb-3 px-1">
        <Image
          src="/img/partners/logoRegion.png"
          alt="La Région Auvergne-Rhîne-Alpes"
          width={225 * 0.7}
          height={75 * 0.7}
        />
      </div>
      <Image
        src="/img/partners/lpo.svg"
        alt="LPO"
        width={225 * 0.75}
        height={75 * 0.75}
      />
    </div>
  );
}
