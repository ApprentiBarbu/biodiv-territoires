import { territoriesStore } from "../_stores/Territories.store";

export const territoriesUtils = {

  getSlug(territoryCode: string, territoryName: string) {
    return `${territoryCode}-${territoryName.toLowerCase().replaceAll(/[\s']/g, "-")}`;
  },

  extractCodeFromSlug(territorySlug: string) {
    return territorySlug.substring(0, territorySlug.indexOf("-"));
  },

  autoComplete(value: string) {
    if (value.trim().length < 1) return [];
    return territoriesStore
      .search(value)
      .then((result) => result.datas);
  },
};
