import React from "react";
import { Autocomplete } from "../../_common/forms/autocomplete/Autocomplete";
import Link from "next/link";
import { TerritorySearchResult } from "../../../shared/territories/_models/territoriesSearch.model";
import { URLS } from "../../../shared/_configs/URLS";
import { Input } from "../../_common/forms/input/Input";
import { BiSearchAlt } from "react-icons/bi";
import styles from "./_css/territoriesSearchField.module.css";
import { territoriesUtils } from "../_utils/Territories.utils";
import { departementsUtils } from "../../../shared/_utils/Departements.utils";

function renderInputIcon(width: number) {
  return <BiSearchAlt size={width}/>;
}

export function TerritoriesSearchField() {
  const [text, setText] = React.useState("");
  const trimmedText = text.trim();

  return (
    <Autocomplete<TerritorySearchResult>
      className={styles.container}
      value={text}
      getSuggestions={territoriesUtils.autoComplete}
      renderSuggestions={(suggestions) => (
        trimmedText.length < 1
          ? <div className="p-2 text-center">Tapez au moins 2 lettres pour lancer la recherche</div>
          : (
            suggestions.length === 0
              ? <div className="p-2 text-center">"Aucun résultat trouvé"</div>
              : (
                suggestions.map((suggestion) => (
                    <div key={suggestion.id} className={styles.suggestionContainer}>
                      <Link href={URLS.territories(territoriesUtils.getSlug(suggestion.area_code, suggestion.area_name))}>
                        <div className={styles.suggestion}>
                          <div>{suggestion.area_name}</div>
                          <div className={styles.suggestionSubtext}>
                            {suggestion.area_code}
                            {" • "}
                            {departementsUtils.getDepartementForAreaCode(suggestion.area_code)}
                          </div>
                        </div>
                      </Link>
                    </div>

                  ),
                )
              )
          )
      )}
    >
      {
        (onOpen) => (
          <Input
            value={text}
            onTextChange={setText}
            placeholder="Commencer par Rechercher une commune"
            iconRenderer={renderInputIcon}
            onFocus={onOpen}
          />
        )
      }
    </Autocomplete>
  );
}
