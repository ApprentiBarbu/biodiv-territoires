import React, { useEffect, useRef, useState } from "react";
import styles from "./_css/searchOverlay.module.css";
import { AnimatePresence, motion } from "framer-motion";
import { Input, INPUT_SIZE, INPUT_THEME } from "../../_common/forms/input/Input";
import { RiCloseLine } from "react-icons/ri";
import { useAutocomplete } from "../../_common/forms/autocomplete/useAutocomplete";
import { territoriesUtils } from "../_utils/Territories.utils";
import { Loader } from "../../_common/loadings/Loader";
import clsx from "clsx";
import { SearchOverlaySuggestion } from "./SearchOverlaySuggestion";
import { BiSearchAlt } from "react-icons/bi";
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";

type Props = {
  open: boolean;
  onClose: () => void;
}

export const SearchOverlay = React.memo((props: Props) => {
  const containerRef = useRef<HTMLDivElement>();
  const [text, setText] = useState("");
  const trimmedText = text.trim();
  const { suggestions, loading } = useAutocomplete(territoriesUtils.autoComplete, trimmedText);
  useEffect(() => {
    const onClose = () => {
      document.body.classList.remove("blur");
      containerRef.current && enableBodyScroll(containerRef.current);
    };

    if (props.open) {
      document.body.classList.add("blur");
      containerRef.current && disableBodyScroll(containerRef.current);
      return onClose;
    } else {
      setText("");
      onClose();
    }
  }, [props.open]);

  return (
    <div ref={containerRef}>
      <AnimatePresence>
        {props.open && (
          <motion.div
            className={styles.container}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            <div className={styles.background}/>
            <motion.div
              className={styles.closeBtn}
              onClick={props.onClose}
              initial={{ top: -50 }}
              animate={{ top: 47 }}
              exit={{ top: -50 }}
              transition={{ delay: 0.2 }}
            >
              <RiCloseLine size={42} color="#2e3a4d"/>
            </motion.div>
            <motion.div
              className={clsx("flex flex-col items-center pt-8 pb-2 max-h-full", styles.inner)}
              initial={{ marginTop: -160 }}
              animate={{ marginTop: 0 }}
              exit={{ marginTop: -160 }}
              transition={{ delay: 0.6 }}
            >
              <div>
                <Input
                  value={text}
                  onTextChange={setText}
                  placeholder="Rechercher un territoire..."
                  iconRenderer={(width) => (
                    <BiSearchAlt size={width} color="#2e3a4d"/>
                  )}
                  autoFocus
                  theme={INPUT_THEME.BLACK}
                  size={INPUT_SIZE.EXTRA_BIG}
                />
              </div>
              <div className="mt-6 z-10 flex-1 overflow-auto flex flex-col items-center w-full">
                <Loader isLoading={loading}>
                  {
                    trimmedText.length < 1
                      ? "Tapez au moins 2 lettres pour lancer la recherche"
                      : (
                        suggestions.length === 0
                          ? "Aucun résultat trouvé"
                          : (
                            suggestions.map((suggestion) => (
                              <SearchOverlaySuggestion
                                key={suggestion.id}
                                suggestion={suggestion}
                                onClick={props.onClose}
                              />
                            ))
                          )
                      )

                  }
                </Loader>
              </div>
            </motion.div>
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
});
