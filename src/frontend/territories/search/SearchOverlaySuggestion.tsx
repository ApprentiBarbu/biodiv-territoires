import React from "react";
import { TerritorySearchResult } from "../../../shared/territories/_models/territoriesSearch.model";
import { departementsUtils } from "../../../shared/_utils/Departements.utils";
import { URLS } from "../../../shared/_configs/URLS";
import { territoriesUtils } from "../_utils/Territories.utils";
import Link from "next/link";
import styles from "./_css/searchOverlaySuggestion.module.css";
import clsx from "clsx";

type Props = {
  suggestion: TerritorySearchResult;
  onClick?: () => void;
};

export function SearchOverlaySuggestion(props: Props) {
  return (
    <Link href={URLS.territories(territoriesUtils.getSlug(props.suggestion.area_code, props.suggestion.area_name))}>
      <div className={clsx(styles.container, "px-2 py-2")} onClick={props.onClick}>
        <div className={styles.name}>{props.suggestion.area_name}</div>
        <div className={styles.location}>
          {props.suggestion.area_code}
          {" • "}
          {departementsUtils.getDepartementForAreaCode(props.suggestion.area_code)}
        </div>
      </div>
    </Link>
  );
}
