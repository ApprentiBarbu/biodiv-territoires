import React, { useContext } from "react";
import { TerritoryStore } from "../_stores/Territory.store";

const TerritoryStoreCtxt = React.createContext<TerritoryStore>({} as TerritoryStore);

type Props = {
  territoryStore: TerritoryStore;
};

export function useTerritoryStore() {
  return useContext(TerritoryStoreCtxt);
}

export function TerritoryStoreContext(props: React.PropsWithChildren<Props>) {
  return (
    <TerritoryStoreCtxt.Provider value={props.territoryStore}>{props.children}</TerritoryStoreCtxt.Provider>
  );
}
