import { fetchUtils } from "../../../shared/_utils/Fetch.utils";
import { TerritoryGetResult } from "../../../shared/territories/_models/territoryGet.model";
import { LoadingState } from "../../_common/loadings/_models/LoadingState.model";
import { loadingUtils } from "../../_common/loadings/_utils/Loading.utils";
import { Territory } from "../../../shared/territories/_models/territories.model";
import { action, observable } from "mobx";
import { TerritoryStatsResult } from "../../../shared/territories/_models/territoryStats.model";
import { TerritoryThreatenedResult } from "../../../shared/territories/_models/territoryThreatened.model";
import { TerritoryTilesResult } from "../../../shared/territories/_models/territoryTiles.model";
import { TerritoryTileSpeciesResult } from "../../../shared/territories/_models/territoryTileSpecies.model";

export class TerritoryStore {

  private fetchLoadingState = new LoadingState<TerritoryGetResult>();
  territory: Territory | undefined;

  stats = new LoadingState<TerritoryStatsResult>();
  threatenedStats: Record<number, LoadingState<TerritoryThreatenedResult>> = observable({});
  tiles: Record<number, LoadingState<TerritoryTilesResult>> = observable({});

  constructor(public territoryCode: string) {
    this.fetch();
  }

  fetch() {
    loadingUtils.fromPromise(
      () => fetchUtils.get<TerritoryGetResult>(`/api/territories/${this.territoryCode}`).then(action(({ data }) => {
        this.territory = data;
        this.fetchStats();
        return data;
      })),
      this.fetchLoadingState,
    );
    return this.fetchLoadingState;
  }

  fetchStats() {
    if (this.territory) {
      loadingUtils.fromPromise(
        () => fetchUtils.get<TerritoryStatsResult>(`/api/territories/${this.territory.id}/stats`).then(({ data }) => data),
        this.stats,
      );
    }
    return this.stats;
  }

  fetchThreatenedStats = action((range: number) => {
    if (!this.threatenedStats[range]) this.threatenedStats[range] = new LoadingState<TerritoryThreatenedResult>();
    if (this.territory) {
      loadingUtils.fromPromise(
        () => fetchUtils.get<TerritoryThreatenedResult>(`/api/territories/${this.territory.id}/threatened?range=${range}`).then(({ data }) => data),
        this.threatenedStats[range],
      );
    }
    return this.threatenedStats;
  });

  fetchTiles = action((range: number) => {
    if (!this.tiles[range]) this.tiles[range] = new LoadingState<TerritoryTilesResult>();
    if (this.territory) {
      loadingUtils.fromPromise(
        () => fetchUtils.get<TerritoryTilesResult>(`/api/territories/${this.territory.id}/tiles?range=${range}`).then(({ data }) => data),
        this.tiles[range],
      );
    }
    return this.tiles;
  });

  fetchTile(tileId: number) {
    return loadingUtils.fromPromise(
      () => fetchUtils.get<TerritoryTileSpeciesResult>(`/api/tiles/${tileId}`).then(({ data }) => data),
    );
  }
}
