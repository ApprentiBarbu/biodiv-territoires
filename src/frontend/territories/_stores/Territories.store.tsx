import { fetchUtils } from "../../../shared/_utils/Fetch.utils";
import { TerritoriesSearchResult } from "../../../shared/territories/_models/territoriesSearch.model";
import { LoadingState } from "../../_common/loadings/_models/LoadingState.model";
import { TerritoriesConfResult } from "../../../shared/territories/_models/territoriesConf.model";
import { loadingUtils } from "../../_common/loadings/_utils/Loading.utils";

class TerritoriesStore {
  conf = new LoadingState<TerritoriesConfResult>();

  async search(text: string) {
    const { data } = await fetchUtils.post<TerritoriesSearchResult>(`/api/territories/search?text=${encodeURIComponent(text)}`);
    return data;
  }

  getConf() {
    return loadingUtils.fromPromise(
      () => fetchUtils.get<TerritoriesConfResult>(`/api/territories/conf`).then(({ data }) => data),
      this.conf,
    );
  }
}

const territoriesStore = new TerritoriesStore();
export { territoriesStore };
