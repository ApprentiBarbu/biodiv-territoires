import React, { useMemo, useState } from "react";
import {
  TerritoryTileSpecie,
  TerritoryTileSpeciesResult,
} from "../../../../../shared/territories/_models/territoryTileSpecies.model";
import { LoadingState } from "../../../../_common/loadings/_models/LoadingState.model";
import { observer } from "mobx-react";
import BaseTable, { Column, ColumnShape, SortOrder } from "react-base-table";
import "react-base-table/styles.css";
import AutoSizer from "react-virtualized-auto-sizer";
import _ from "lodash";
import styles from "./_css/speciesTable.module.css";

type Props = {
  loadingState: LoadingState<TerritoryTileSpeciesResult>
};

const COLUMNS: ColumnShape<TerritoryTileSpecie>[] = [
  {
    key: "nom_vern",
    sortable: true,
    dataKey: "nom_vern",
    title: "Nom vernaculaire",
    width: 150,
    frozen: Column.FrozenDirection.LEFT,
  },
  {
    key: "group2_inpn",
    sortable: true,
    dataKey: "group2_inpn",
    title: "Groupe 2 INPN",
    width: 120,
  },
  {
    key: "lb_nom",
    sortable: true,
    dataKey: "lb_nom",
    title: "Nom scientifique",
    width: 200,
  },
  {
    key: "count_dataset",
    sortable: true,
    dataKey: "count_dataset",
    title: "Nombre d'occurences",
    width: 150,
  },
  {
    key: "count_observer",
    sortable: true,
    dataKey: "count_observer",
    title: "Nombre d'observateurs",
    width: 150,
  },
  {
    key: "count_date",
    sortable: true,
    dataKey: "count_date",
    title: "Nombre de date",
    width: 150,
  },
  {
    key: "last_year",
    sortable: true,
    dataKey: "last_year",
    title: "Dernière observation",
    width: 150,
  },
];

export const SpeciesTable = observer((props: Props) => {
  const data = props.loadingState.value;
  const [sort, setSort] = useState<{ key: React.Key; order: SortOrder; }>({
    key: "count_observer",
    order: "desc",
  });
  const sortedData = useMemo(() => data ? _.orderBy(data.data, sort.key, sort.order) : undefined, [data, sort]);
  if (!sortedData) return null;

  return (
    <AutoSizer disableHeight>
      {({ width }) => (
        <BaseTable
          className={styles.container}
          fixed
          columns={COLUMNS}
          width={width}
          height={500}
          data={sortedData}
          sortBy={sort}
          onColumnSort={setSort}
        />
      )}
    </AutoSizer>
  );
});
