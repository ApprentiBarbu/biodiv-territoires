import React, { useMemo } from "react";
import { TerritoryStore } from "../_stores/Territory.store";
import { TerritoryStoreContext } from "../_contexts/TerritoryStore.context";
import { LoaderObservable } from "../../_common/loadings/LoaderObservable";
import { TerritoryHeader } from "./header/TerritoryHeader";
import { ThreatenedSpecies } from "./threatened/ThreatenedSpecies";
import styles from "./_css/territoryScreen.module.css";
import { ObservationsNoSsr } from "./observations/ObservationsNoSsr";
import { SpeciesList } from "./speciesList/SpeciesList";
import { SurroundingsList } from "./surroundingsList/SurroundingsList";

type Props = {
  territoryCode: string;
};

export function TerritoryScreen(props: Props) {
  const territoryStore = useMemo(() => new TerritoryStore(props.territoryCode), [props.territoryCode]);

  return (
    <TerritoryStoreContext territoryStore={territoryStore}>
      <div className={styles.container}>
        <LoaderObservable
          loadingState={territoryStore.fetch()}
          onRetry={() => territoryStore.fetch()}
        >
          <TerritoryHeader/>
          <div className="mb-20">
            <ThreatenedSpecies/>
          </div>
          <div className="mb-20">
            <ObservationsNoSsr/>
          </div>
          <div className="mb-20">
            <SpeciesList/>
          </div>
          <div className="mb-20">
            <SurroundingsList/>
          </div>
        </LoaderObservable>
      </div>
    </TerritoryStoreContext>
  );
}
