import React from "react";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import { observer } from "mobx-react";
import _ from "lodash";
import { BarChart } from "../../../_common/charts/BarChart";

type Props = {
  range: number;
}

export const ThreatenedSpeciesGlobalChart = observer((props: Props) => {
  const territoryStore = useTerritoryStore();
  if (!territoryStore.territory) return null;

  const threatenedStats = territoryStore.threatenedStats[props.range].value;

  return (
    <BarChart
      title="Nombre d'espèces observés par catégories de menaces"
      options={{
        data: {
          labels: ["sur le territoire", "autour du territoire"],
          datasets: [
            {
              backgroundColor: "#FF4A1C",
              data: [
                _.sum(threatenedStats.territory.threatened),
                _.sum(threatenedStats.surrounding.threatened),
              ],
              label: "espèces menacées",
            },
            {
              backgroundColor: "#0C86C7",
              data: [
                _.sum(threatenedStats.territory.not_threatened),
                _.sum(threatenedStats.surrounding.not_threatened),
              ],
              label: "autres espèces",
            },
          ],
        },
      }}
    />
  );
});
