import React from "react";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import { observer } from "mobx-react";
import { BarChart } from "../../../_common/charts/BarChart";

type Props = {
  range: number;
}

export const ThreatenedSpeciesChartBySpecies = observer((props: Props) => {
  const territoryStore = useTerritoryStore();
  if (!territoryStore.territory) return null;

  const threatenedStats = territoryStore.threatenedStats[props.range].value;

  return (
    <BarChart
      title="Nombre d'espèces observés par catégories de menaces et par groupe taxonomique"
      options={{
        data: {
          labels: threatenedStats.labels,
          datasets: [
            {
              backgroundColor: "#FF4A1C",
              data: threatenedStats.surrounding.threatened,
              label: "espèces menacées",
            },
            {
              backgroundColor: "#0C86C7",
              data: threatenedStats.surrounding.not_threatened,
              label: "autres espèces",
            },
          ],
        },
      }}
    />
  );
});
