import React, { useEffect, useState } from "react";
import { Title } from "../../../_common/layout/Title";
import { ThreatenedSpeciesGlobalChart } from "./ThreatenedSpeciesGlobalChart";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import { LoaderObservable } from "../../../_common/loadings/LoaderObservable";
import { observer } from "mobx-react";
import { RangeSelector } from "../RangeSelector";
import { ThreatenedSpeciesChartBySpecies } from "./ThreatenedSpeciesChartBySpecies";
import { ImWarning } from "react-icons/im";

export const ThreatenedSpecies = observer(() => {
  const [range, setRange] = useState(2000);
  const territoryStore = useTerritoryStore();
  useEffect(() => {
    territoryStore.fetchThreatenedStats(range);
  }, [territoryStore, range]);
  if (!territoryStore.territory) return null;

  return (
    <div>
      <Title
        className="mb-8"
        line1="Espèces menacées"
        line2="du territoire"
        icon={<ImWarning size={16}/>}
      />
      <div className="mb-4">
        <RangeSelector range={range} onChange={setRange} theme="inline"/>
      </div>
      <LoaderObservable
        loadingState={territoryStore.threatenedStats[range]}
        onRetry={() => territoryStore.fetchThreatenedStats(range)}
      >
        <div className="grid grid-cols-2 gap-20">
          <div>
            <ThreatenedSpeciesGlobalChart range={range}/>
          </div>
          <div>
            <ThreatenedSpeciesChartBySpecies range={range}/>
          </div>
        </div>
      </LoaderObservable>
    </div>
  );
});
