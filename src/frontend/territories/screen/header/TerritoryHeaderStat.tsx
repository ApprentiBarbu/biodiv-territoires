import React, { ReactNode } from "react";
import styles from "./_css/territoryHeaderStat.module.css";
import clsx from "clsx";
import { Loader } from "../../../_common/loadings/Loader";
import { Skeleton } from "../../../_common/loadings/Skeleton";

type Props = {
  value?: string;
  label: string;
  icon: ReactNode;
};

export function TerritoryHeaderStat(props: Props) {
  return (
    <div className={clsx("m-1 px-6", styles.container)}>
      <div className="flex flex-row items-center">
        <div className="mr-2">{props.icon}</div>
        <Loader
          isLoading={props.value === undefined}
          spinner={<Skeleton width={150}/>}
        >
          <div className="font-bold text-lg">{props.value}</div>
        </Loader>
      </div>
      <div className="-mt-1 opacity-75 text-lg">{props.label}</div>
    </div>
  );
}
