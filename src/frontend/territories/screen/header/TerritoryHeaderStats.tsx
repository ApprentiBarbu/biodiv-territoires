import React from "react";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import { TerritoryHeaderStat } from "./TerritoryHeaderStat";
import { ImBinoculars, ImWarning } from "react-icons/im";
import { numberUtils } from "../../../../shared/_utils/Number.utils";
import { FaKiwiBird } from "react-icons/fa";
import { RiCalendarCheckLine, RiCalendarLine, RiUserFill } from "react-icons/ri";
import dayjs from "dayjs";
import { observer } from "mobx-react";

export const TerritoryHeaderStats = observer(() => {
  const territoryStore = useTerritoryStore();
  if (!territoryStore.territory || !territoryStore.stats.isSucceeded.get()) return null;

  const lastObservation = territoryStore.stats.value
    ? dayjs(territoryStore.stats.value.lastObservation).format("DD/MM/YYYY")
    : undefined;

  return (
    <div className="flex flex-row flex-wrap items-center">
      <TerritoryHeaderStat
        value={numberUtils.formatNumber(territoryStore.stats.value?.observations)}
        label="observations"
        icon={<ImBinoculars size={17}/>}
      />
      <TerritoryHeaderStat
        value={numberUtils.formatNumber(territoryStore.stats.value?.species)}
        label="espèces"
        icon={<FaKiwiBird size={18}/>}
      />
      <TerritoryHeaderStat
        value={numberUtils.formatNumber(territoryStore.stats.value?.threatenedSpecies)}
        label="espèces menacées"
        icon={<ImWarning size={19}/>}
      />
      <TerritoryHeaderStat
        value={numberUtils.formatNumber(territoryStore.stats.value?.observers)}
        label="observateurs"
        icon={<RiUserFill size={20}/>}
      />
      <TerritoryHeaderStat
        value={numberUtils.formatNumber(territoryStore.stats.value?.observedDates)}
        label="dates d'observations"
        icon={<RiCalendarLine size={20}/>}
      />
      <TerritoryHeaderStat
        value={lastObservation}
        label="dernière observation"
        icon={<RiCalendarCheckLine size={20}/>}
      />
    </div>
  );
});
