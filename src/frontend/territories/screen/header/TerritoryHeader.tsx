import React from "react";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import styles from "./_css/territoryHeader.module.css";
import clsx from "clsx";
import { departementsUtils } from "../../../../shared/_utils/Departements.utils";
import { TerritoryHeaderStats } from "./TerritoryHeaderStats";

export function TerritoryHeader() {
  const territoryStore = useTerritoryStore();
  if (!territoryStore.territory) return null;

  return (
    <div className={clsx(styles.container, "sticky flex flex-row items-center mb-10 justify-between")}>
      <div className="my-2">
        <h1 className="text-3xl">{territoryStore.territory.area_name}</h1>
        <div className="font-bold opacity-75 text-lg">
          {territoryStore.territory.area_code}
          {" • "}
          {departementsUtils.getDepartementForAreaCode(territoryStore.territory.area_code)}
        </div>
      </div>
      <TerritoryHeaderStats/>
    </div>
  );
}
