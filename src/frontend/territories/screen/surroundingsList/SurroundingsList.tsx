import React, { useEffect, useState } from "react";
import { Title } from "../../../_common/layout/Title";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import { LoaderObservable } from "../../../_common/loadings/LoaderObservable";
import { observer } from "mobx-react";
import { RangeSelector } from "../RangeSelector";

export const SurroundingsList = observer(() => {
  const [range, setRange] = useState(2000);
  const territoryStore = useTerritoryStore();
  useEffect(() => {
    territoryStore.fetchThreatenedStats(range);
  }, [territoryStore, range]);
  if (!territoryStore.territory) return null;

  return (
    <div>
      <Title
        className="mb-8"
        line1="Données de synthèse"
        line2="sur les territoires alentours"
      />
      <div className="mb-4">
        <RangeSelector range={range} onChange={setRange} theme="inline"/>
      </div>
      <LoaderObservable
        loadingState={territoryStore.threatenedStats[range]}
        onRetry={() => territoryStore.fetchThreatenedStats(range)}
      >
        <div>TODO</div>
      </LoaderObservable>
    </div>
  );
});
