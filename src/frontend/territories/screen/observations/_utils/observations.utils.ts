import {
  TerritoriesCategoriesConf,
  TerritoriesCategoryConf,
  TerritoriesConfResult,
} from "../../../../../shared/territories/_models/territoriesConf.model";
import Color from "color";
import { OBSERVATIONS_DATA_LAYERS } from "../map/_models/observationsDataLayer.model";

const memoizedCategories: Record<string, TerritoriesCategoriesConf | undefined> = {};

export const TERRITORY_CATEGORY_MAX_MAX_VALUE = 534;

export const observationsUtils = {

  getCategoriesForDataLayerId(conf: TerritoriesConfResult, dataLayerId: string): TerritoriesCategoriesConf {
    let categories = memoizedCategories[dataLayerId];
    if (!categories) {
      categories = conf.filter((category) => category.type === dataLayerId);
      memoizedCategories[dataLayerId] = categories;
    }
    return categories;
  },

  getColor(conf: TerritoriesConfResult, dataLayerId: string, value: number) {
    const dataLayer = OBSERVATIONS_DATA_LAYERS[dataLayerId];
    const categories = observationsUtils.getCategoriesForDataLayerId(conf, dataLayerId);
    const categoryForValue = categories.find((category) => category.min <= value && (category.max === TERRITORY_CATEGORY_MAX_MAX_VALUE || category.max >= value));
    return observationsUtils.computeColor(dataLayer.color, categoryForValue, categories.length).toString();
  },

  computeColor(baseColor: string, categoryConf: TerritoriesCategoryConf | undefined, nbCategories: number) {
    return Color(baseColor).lighten(categoryConf ? 1 - (categoryConf.ntile / nbCategories) : 0);
  },
};
