import React from "react";
import { territoriesStore } from "../../../../_stores/Territories.store";
import { observationsUtils } from "../../_utils/observations.utils";
import { ObservationsLegend } from "./ObservationsLegend";
import styles from "./_css/observationsLegends.module.css";

type Props = {
  dataLayerId: string;
};

export const ObservationsLegends = React.memo((props: Props) => {
  const conf = territoriesStore.getConf();
  if (!conf.value) return null;
  const categories = observationsUtils.getCategoriesForDataLayerId(conf.value, props.dataLayerId);

  return (
    <div className={styles.container}>
      {categories.map((category) => (
        <ObservationsLegend
          key={category.id}
          dataLayerId={props.dataLayerId}
          category={category}
          nbCategories={categories.length}
        />
      ))}
    </div>
  );
});
