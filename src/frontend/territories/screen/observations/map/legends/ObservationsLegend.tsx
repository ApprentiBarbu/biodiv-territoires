import React from "react";
import { TerritoriesCategoryConf } from "../../../../../../shared/territories/_models/territoriesConf.model";
import { observationsUtils, TERRITORY_CATEGORY_MAX_MAX_VALUE } from "../../_utils/observations.utils";
import styles from "./_css/observationsLegend.module.css";
import { OBSERVATIONS_DATA_LAYERS } from "../_models/observationsDataLayer.model";

type Props = {
  dataLayerId: string;
  category: TerritoriesCategoryConf;
  nbCategories: number;
};

export function ObservationsLegend(props: Props) {
  const dataLayer = OBSERVATIONS_DATA_LAYERS[props.dataLayerId];
  const color = observationsUtils.computeColor(dataLayer.color, props.category, props.nbCategories);
  return (
    <div className="flex flex-row px-2 py-0.5 items-center">
      <div className={styles.colorBlock} style={{ backgroundColor: color.toString() }}/>
      <div>
        {props.category.min}{props.category.max >= TERRITORY_CATEGORY_MAX_MAX_VALUE ? "+" : ` - ${props.category.max}`}
      </div>
    </div>
  );
}
