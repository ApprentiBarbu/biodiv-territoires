import React, { useState } from "react";
import "leaflet/dist/leaflet.css";
import { LatLngExpression } from "leaflet";
import { BaseMap } from "../../../../_common/map/BaseMap";
import { useTerritoryStore } from "../../../_contexts/TerritoryStore.context";
import { Dropdown } from "../../../../_common/forms/dropdown/Dropdown";
import styles from "./_css/observationsMap.module.css";
import { OBSERVATIONS_DATA_LAYERS_ARRAY, ObservationsDataLayers } from "./_models/observationsDataLayer.model";
import { ObservationsLegends } from "./legends/ObservationsLegends";
import { territoriesStore } from "../../../_stores/Territories.store";
import { TerritoryTileProperties } from "../../../../../shared/territories/_models/territoryTiles.model";
import { ObservationsDataLayer } from "./ObservationsDataLayer";

export type ObservationsMapProps = {
  range: number;
  onFeatureSelect: (properties: TerritoryTileProperties) => void;
};

const POSITION: LatLngExpression = [45.761891, 5.143159];

export const ObservationsMap = React.memo((props: ObservationsMapProps) => {
  const conf = territoriesStore.getConf();
  const territoryStore = useTerritoryStore();
  const [selectedDataLayerId, setSelectedDataLayerId] = useState(OBSERVATIONS_DATA_LAYERS_ARRAY[0].id);

  if (!territoryStore.territory || !conf.value) return null;

  return (
    <BaseMap
      center={POSITION}
      controlChildren={
        <>
          <div className={styles.dataLayerSelector}>
            <Dropdown<ObservationsDataLayers>
              value={selectedDataLayerId}
              options={OBSERVATIONS_DATA_LAYERS_ARRAY.map((layer) => ({ value: layer.id, label: layer.title }))}
              onChange={setSelectedDataLayerId}
            />
          </div>
          <div className={styles.dataLayerLegends}>
            <ObservationsLegends dataLayerId={selectedDataLayerId}/>
          </div>
        </>
      }
    >
      <ObservationsDataLayer
        range={props.range}
        selectedDataLayerId={selectedDataLayerId}
        onFeatureSelect={props.onFeatureSelect}
      />
    </BaseMap>
  );
});
