export enum ObservationsDataLayers {
  TAXA = "taxa",
  OCCTAX = "occtax",
  THREATENED = "threatened",
  OBSERVER = "observer",
  DATE = "date",
}

export type ObservationDataLayer = {
  id: ObservationsDataLayers,
  property: "count_taxa" | "count_occtax" | "count_threatened" | "count_observer" | "count_date",
  color: string,
  title: string,
  text: string,
}

export const OBSERVATIONS_DATA_LAYERS: Record<ObservationsDataLayers, ObservationDataLayer> = {
  taxa: {
    id: ObservationsDataLayers.TAXA,
    property: "count_taxa",
    color: "#af3d56",
    title: "Nombre de taxons",
    text: "taxons",
  },
  occtax: {
    id: ObservationsDataLayers.OCCTAX,
    property: "count_occtax",
    color: "#0C86C7",
    title: "Nombre d'observations",
    text: "observations",
  },
  threatened: {
    id: ObservationsDataLayers.THREATENED,
    property: "count_threatened",
    color: "#dc3e17",
    title: "Nombre de taxons menacés",
    text: "taxons menacés",
  },
  observer: {
    id: ObservationsDataLayers.OBSERVER,
    property: "count_observer",
    color: "#579756",
    title: "Nombre d'observateurs",
    text: "observateurs",
  },
  date: {
    id: ObservationsDataLayers.DATE,
    property: "count_date",
    color: "#81523F",
    title: "Nombre de dates d'observations",
    text: "dates d'observations",
  },
};

export const OBSERVATIONS_DATA_LAYERS_ARRAY = Object.values(OBSERVATIONS_DATA_LAYERS);
