import React, { useRef } from "react";
import { GeoJSON, useMap } from "react-leaflet";
import { observationsUtils } from "../_utils/observations.utils";
import { TerritoryTileProperties } from "../../../../../shared/territories/_models/territoryTiles.model";
import { OBSERVATIONS_DATA_LAYERS, ObservationsDataLayers } from "./_models/observationsDataLayer.model";
import { territoriesStore } from "../../../_stores/Territories.store";
import { useTerritoryStore } from "../../../_contexts/TerritoryStore.context";
import { GeoJSON as TGeoJSON } from "leaflet";
import { ObservationsPopup } from "./ObservationsPopup";
import ReactDOM from "react-dom";
import styles from "./_css/observationsDataLayer.module.css";

type Props = {
  range: number;
  selectedDataLayerId: ObservationsDataLayers;
  onFeatureSelect: (properties: TerritoryTileProperties) => void;
};

const memoizedPopups: Record<string, Promise<string>> = {};

function renderPopup(properties: TerritoryTileProperties) {
  if (!memoizedPopups[properties.id_area]) {
    const popupDiv = document.createElement("div");
    memoizedPopups[properties.id_area] = new Promise((resolve) => {
      ReactDOM.render(<ObservationsPopup properties={properties}/>, popupDiv, () => {
        resolve(popupDiv.innerHTML);
      });
    });
  }
  return memoizedPopups[properties.id_area];
}

export const ObservationsDataLayer = React.memo((props: Props) => {
  const layerRef = useRef<TGeoJSON<any>>();
  const featureSelectedRef = useRef<TerritoryTileProperties>();
  const map = useMap();

  const conf = territoriesStore.getConf();
  const territoryStore = useTerritoryStore();
  const dataLayer = OBSERVATIONS_DATA_LAYERS[props.selectedDataLayerId];
  const geoJsonData = territoryStore.tiles[props.range].value;

  React.useEffect(() => {
    if (layerRef.current) map.fitBounds(layerRef.current.getBounds());
  }, [geoJsonData, map]);

  return (
    <GeoJSON
      ref={(ref) => {
        layerRef.current = ref;
        if (layerRef.current) map.fitBounds(layerRef.current.getBounds());
      }}
      key={props.range + props.selectedDataLayerId}
      data={geoJsonData}
      style={(feature) => {
        const value = feature.properties[dataLayer.property];
        const isSelected = featureSelectedRef.current === feature.properties;
        const borderColor = isSelected ? "#0C86C7" : "#FFF";
        return {
          stroke: true,
          color: borderColor,
          weight: isSelected ? 3 : 1,
          fillColor: observationsUtils.getColor(conf.value, props.selectedDataLayerId, value),
          fillOpacity: 0.8,
        };
      }}
      onEachFeature={(feature, layer) => {
        layer.on({
          click: () => {
            props.onFeatureSelect(feature.properties);
            featureSelectedRef.current = feature.properties;
            layerRef.current.resetStyle();
          },
        });
        renderPopup(feature.properties).then((popupHtml) => layer.bindPopup(popupHtml, {
          className: styles.popupWrapper,
          closeButton: false,
        }));
      }}
    />
  );
});
