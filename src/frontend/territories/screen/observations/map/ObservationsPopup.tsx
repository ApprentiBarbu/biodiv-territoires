import React from "react";
import { TerritoryTileProperties } from "../../../../../shared/territories/_models/territoryTiles.model";
import { OBSERVATIONS_DATA_LAYERS_ARRAY } from "./_models/observationsDataLayer.model";

type Props = {
  properties: TerritoryTileProperties;
};

export function ObservationsPopup(props: Props) {
  return (
    <div>
      {OBSERVATIONS_DATA_LAYERS_ARRAY.map((dataLayer) => (
        <div key={dataLayer.id} className="flex flex-row my-0.5 text-xs">
          <div className="font-bold">{props.properties[dataLayer.property]}</div>
          <div className="ml-1">{dataLayer.text}</div>
        </div>
      ))}
    </div>
  );
}
