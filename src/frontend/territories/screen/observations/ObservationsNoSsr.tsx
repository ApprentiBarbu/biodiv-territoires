import dynamic from "next/dynamic";

export function ObservationsNoSsr() {
  const Observations = dynamic(
    () => import("./Observations"),
    { ssr: false },
  );
  return <Observations/>;
}
