import React, { useEffect, useState } from "react";
import { Title } from "../../../_common/layout/Title";
import { useTerritoryStore } from "../../_contexts/TerritoryStore.context";
import { LoaderObservable } from "../../../_common/loadings/LoaderObservable";
import { observer } from "mobx-react";
import { RangeSelector } from "../RangeSelector";
import { ObservationsList } from "./list/ObservationsList";
import { territoriesStore } from "../../_stores/Territories.store";
import { TerritoryTileProperties } from "../../../../shared/territories/_models/territoryTiles.model";
import { ObservationsMap } from "./map/ObservationsMap";
import { ImBinoculars } from "react-icons/im";

const Observations = observer(() => {
  const [range, setRange] = useState(2000);
  const [selectedFeature, setSelectedFeature] = useState<TerritoryTileProperties>();
  const territoryStore = useTerritoryStore();
  useEffect(() => {
    territoryStore.fetchTiles(range);
  }, [territoryStore, range]);
  const conf = territoriesStore.getConf();

  if (!territoryStore.territory) return null;

  return (
    <div>
      <Title
        className="mb-8"
        line1="Répartition des"
        line2="observations"
        icon={<ImBinoculars size={16}/>}
      />
      <div className="mb-4">
        <RangeSelector range={range} onChange={setRange} theme="inline"/>
      </div>
      <LoaderObservable
        loadingState={conf}
        onRetry={() => territoriesStore.getConf()}
      >
        <LoaderObservable
          loadingState={territoryStore.tiles[range]}
          onRetry={() => territoryStore.fetchTiles(range)}
        >
          <div className="grid grid-cols-2 gap-4">
            <div>
              <ObservationsMap range={range} onFeatureSelect={setSelectedFeature}/>
            </div>
            <div>
              <ObservationsList selectedFeature={selectedFeature}/>
            </div>
          </div>
        </LoaderObservable>
      </LoaderObservable>
    </div>
  );
});

export default Observations;