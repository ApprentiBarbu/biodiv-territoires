import React, { useEffect, useState } from "react";
import { TerritoryTileProperties } from "../../../../../shared/territories/_models/territoryTiles.model";
import { SpeciesTable } from "../../_common/table/SpeciesTable";
import { LoadingState } from "../../../../_common/loadings/_models/LoadingState.model";
import { useTerritoryStore } from "../../../_contexts/TerritoryStore.context";
import { LoaderObservable } from "../../../../_common/loadings/LoaderObservable";
import { TerritoryTileSpeciesResult } from "../../../../../shared/territories/_models/territoryTileSpecies.model";
import { BsInfoSquare } from "react-icons/bs";

type Props = {
  selectedFeature?: TerritoryTileProperties;
}

export function ObservationsList(props: Props) {
  const territoryStore = useTerritoryStore();
  const [loadingState, setLoadingState] = useState<LoadingState<TerritoryTileSpeciesResult>>();

  useEffect(() => {
    setLoadingState(props.selectedFeature ? territoryStore.fetchTile(props.selectedFeature.id_area) : undefined);
  }, [props.selectedFeature]);

  return (
    <div>
      {
        props.selectedFeature
          ? (
            <LoaderObservable
              loadingState={loadingState}
              onRetry={() => setLoadingState(territoryStore.fetchTile(props.selectedFeature.id_area))}
            >
              <SpeciesTable loadingState={loadingState}/>
            </LoaderObservable>
          )
          : (
            <div className="flex flex-col items-center p-2">
              <div className="flex flex-row items-center">
                <BsInfoSquare size={32}/>
                <div className="ml-6 max-w-sm">
                  Cliquez sur une maille de la carte pour accéder à la lite des espèces observées.
                </div>
              </div>
            </div>
          )
      }
    </div>
  );
}
