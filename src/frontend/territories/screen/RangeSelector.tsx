import React from "react";
import styles from "./_css/rangeSelector.module.css";
import { Dropdown } from "../../_common/forms/dropdown/Dropdown";
import clsx from "clsx";

type Props = {
  range: number;
  onChange: (range: number) => void;
  theme?: "inline" | "default";
};

const RANGES = [
  { value: 1000, label: "1 km" },
  { value: 2000, label: "2 km" },
  { value: 5000, label: "5 km" },
  { value: 10000, label: "10 km" },
];

export function RangeSelector(props: Props) {
  return (
    <div className={clsx(styles.container, { "flex flex-row": props.theme === "inline" })}>
      <div className="mb-1 mr-2">Distance tampon</div>
      <Dropdown
        value={props.range}
        onChange={props.onChange}
        options={RANGES}
        theme={props.theme}
      />
    </div>
  );
}
