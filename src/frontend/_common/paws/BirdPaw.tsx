import React from "react";

type Props = {};

export function BirdPaw(props: Props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="5.242125mm"
      height="5.3559017mm"
      viewBox="0 0 5.242125 5.3559017"
      version="1.1"
      style={{ strokeLinecap: "round", stroke: "#2E3A4D", strokeLinejoin: "round" }}
    >
      <g transform="translate(493.17822,-140.62748)">
        <path d="m -490.75485,145.71503 0.0945,-4.8192"/>
        <path d="m -492.92606,141.7174 2.23351,2.35902"/>
        <path d="m -488.2035,141.84078 -2.50409,2.2206"/>
      </g>
    </svg>
  );
}
