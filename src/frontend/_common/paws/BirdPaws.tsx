import React from "react";
import styles from "./_css/birdPaws.module.css";

type Props = {
  animate?: boolean;
  color?: string;
  className?: string;
};

const NB_PAWS = 14;

export function BirdPaws(props: Props) {
  const [pawsDisplayed, setPawsDisplayed] = React.useState(props.animate ? 0 : NB_PAWS);

  React.useEffect(() => {
    if (!props.animate) {
      setPawsDisplayed(NB_PAWS);
    } else if (pawsDisplayed < NB_PAWS) {
      const timeoutId = setTimeout(() => {
        setPawsDisplayed(pawsDisplayed + 1);
      }, 800);
      return () => clearTimeout(timeoutId);
    }
  }, [props.animate, pawsDisplayed]);

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={`${41.2 * 1.2}mm`}
      height={`${15.5 * 1.2}mm`}
      viewBox="0 0 83.424133 30.920557"
      version="1.1"
      style={{ strokeLinecap: "round", stroke: props.color ?? "#2E3A4D", strokeLinejoin: "round" }}
      className={props.className}
    >
      <g transform="translate(662.14289,-39.886274)">
        {pawsDisplayed > 0 && <path
          className={styles.path}
          d="m -660.02951,53.178521 -0.48765,-3.311148 m 3.176,-0.573514 -3.19682,0.577867 m -1.33721,-0.94911 3.98134,2.717077"/>}
        {pawsDisplayed > 1 && <path
          className={styles.path}
          d="m -658.23263,61.256645 -1.0401,-3.181158 m 3.03344,-1.101884 -3.05322,1.109691 m -1.47835,-0.709527 4.38319,2.005337"/>}
        {pawsDisplayed > 2 && <path
          className={styles.path}
          d="m -646.3712,58.686483 -0.93296,-3.214195 m 3.06865,-0.999668 -3.08868,1.006806 m -1.45375,-0.758649 4.31355,2.151019"/>}
        {pawsDisplayed > 3 && <path
          className={styles.path}
          d="m -644.89268,66.687377 -1.00068,-3.19378 m 3.04684,-1.064279 -3.06671,1.071843 m -1.46946,-0.727759 4.35805,2.059397"/>}
        {pawsDisplayed > 4 && <path
          className={styles.path}
          d="m -632.45726,62.389401 -1.46734,-3.008047 m 2.85326,-1.508258 -2.87178,1.518709 m -1.56182,-0.499666 4.61715,1.38403"/>}
        {pawsDisplayed > 5 && <path
          className={styles.path}
          d="m -628.96511,70.539531 -2.32547,-2.407025 m 2.24716,-2.3165 -2.26154,2.33216 m -1.63978,0.0077 4.81885,-0.111265"/>}
        {pawsDisplayed > 6 && <path
          className={styles.path}
          d="m -619.20865,62.624829 -2.49387,-2.232038 m 2.07326,-2.473368 -2.08647,2.490034 m -1.63491,0.126573 4.79809,-0.46042"/>}
        {pawsDisplayed > 7 && <path
          className={styles.path}
          d="m -613.28124,70.133516 -2.74957,-1.90825 m 1.7532,-2.709652 -1.76426,2.727812 m -1.60684,0.327075 4.70499,-1.047264"/>}
        {pawsDisplayed > 8 && <path
          className={styles.path}
          d="m -606.05038,59.112643 -3.24513,-0.818888 m 0.68779,-3.153236 -0.69174,3.174126 m -1.38922,0.871218 4.03572,-2.635618"/>}
        {pawsDisplayed > 9 && <path
          className={styles.path}
          d="m -598.40957,65.428643 -3.26903,-0.71761 m 0.58942,-3.173083 -0.59274,3.194087 m -1.36128,0.914251 3.95185,-2.759818"/>}
        {pawsDisplayed > 10 && <path
          className={styles.path}
          d="m -596.74257,51.605819 -3.33338,-0.299994 m 0.18492,-3.222067 -0.18555,3.243318 m -1.23545,1.078247 3.5726,-3.235745"/>}
        {pawsDisplayed > 11 && <path
          className={styles.path}
          d="m -587.7936,56.667529 -3.3309,-0.326598 m 0.21064,-3.220479 -0.21145,3.241734 m -1.24381,1.068588 3.59834,-3.20713"/>}
        {pawsDisplayed > 12 && <path
          className={styles.path}
          d="m -587.83087,43.67668 -3.33338,-0.3 m 0.18492,-3.22206 -0.18555,3.24331 m -1.23545,1.07825 3.5726,-3.23574"/>}
        {pawsDisplayed > 13 && <path
          className={styles.path}
          d="m -578.98711,48.254579 -3.34569,0.08906 m -0.19043,-3.221728 0.19226,3.242925 m -1.10166,1.21461 3.17277,-3.628673"/>}
      </g>
    </svg>
  );
}
