import React, { useEffect } from "react";
import { BirdPaw } from "./BirdPaw";

type Props = {};

type State = {
  speed: number;
  rotation: number;
}

type PawPosition = {
  rotation: number;
  top: number;
  left: number;
}

function generateNextPaw(state: State, paws: PawPosition[]): [PawPosition[], State] {
  if (paws.length === 0) {
    const nextPaws = [{ rotation: state.rotation, top: 0, left: 0 }];
    return [nextPaws, state];
  }

  const lastPaw = paws[paws.length - 1];
  const rightFoot = paws.length % 2 === 1;
  const radian = (rightFoot ? 90 : state.rotation) * Math.PI / 180;
  const distance = rightFoot ? 15 : 50;

  const nextPaw = {
    rotation: state.rotation,
    // top: (lastPaw.top + distance) * Math.cos(radian) - (lastPaw.left + distance) * Math.sin(radian),
    // left: (lastPaw.top + distance) * Math.sin(radian) + (lastPaw.left + distance) * Math.cos(radian),
    top: lastPaw.top + distance,
    left: lastPaw.left + (24 * (rightFoot ? 1 : -1)),
  };

  let nextPaws = [...paws, nextPaw];
  // if (nextPaws.length > 10) nextPaws.shift();

  let nextState = { ...state };
  if (rightFoot) {
    nextState.rotation -= Math.round(Math.random() * 50) - 25;
  }

  return [nextPaws, nextState];
}

export function BirdPawsPath(props: Props) {
  const [state, setState] = React.useState({ speed: 1, rotation: 0 });
  const [paws, setPaws] = React.useState<PawPosition[]>([]);

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      const [nextPaws, nextState] = generateNextPaw(state, paws);
      setPaws(nextPaws);
      setState(nextState);
    }, 1000);
    return () => clearTimeout(timeoutId);
  }, [paws]);

  return (
    <div style={{ position: "relative" }}>
      {paws.map((paw, index) => (
        <div
          key={index}
          className="absolute"
          style={{ top: paw.top, left: paw.left, transform: `rotate(${180 + paw.rotation}deg)` }}
        >
          <BirdPaw/>
        </div>
      ))}
    </div>
  );
}
