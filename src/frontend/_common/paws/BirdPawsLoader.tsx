import React from "react";
import styles from "./_css/birdPaws.module.css";

type Props = {
  animate?: boolean;
  color?: string;
  className?: string;
};

const NB_PAWS = 5;

export function BirdPawsLoader(props: Props) {
  const [pawsDisplayed, setPawsDisplayed] = React.useState(props.animate ? 1 : NB_PAWS);

  React.useEffect(() => {
    if (!props.animate) {
      setPawsDisplayed(NB_PAWS);
    } else {
      const timeoutId = setTimeout(() => {
        setPawsDisplayed(pawsDisplayed === NB_PAWS ? 1 : pawsDisplayed + 1);
      }, 650);
      return () => clearTimeout(timeoutId);
    }
  }, [props.animate, pawsDisplayed]);

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={`${25.68 * 0.6}mm`}
      height={`${16.46 * 0.6}mm`}
      viewBox="0 0 25.6809 16.459143"
      version="1.1"
      style={{ strokeLinecap: "round", stroke: props.color ?? "#2E3A4D", strokeLinejoin: "round" }}
      className={props.className}
    >
      <g transform="translate(618.38444,-88.010451)">
        {pawsDisplayed > 0 && <path
          className={styles.path}
          d="m -615.25511,92.714543 -1.30851,-3.080472 m 2.92801,-1.357436 -2.94706,1.366909 m -1.53364,-0.580414 4.53871,1.622869"/>}
        {pawsDisplayed > 1 && <path
          className={styles.path}
          d="m -612.8075,100.55618 -0.84392,-3.238732 m 3.09506,-0.914606 -3.11527,0.921189 m -1.43227,-0.79847 4.25257,2.269233"/>}
        {pawsDisplayed > 2 && <path
          className={styles.path}
          d="m -604.58949,96.36264 -1.79293,-2.826105 m 2.66772,-1.816366 -2.68496,1.82881 m -1.60772,-0.322782 4.74248,0.861696"/>}
        {pawsDisplayed > 3 && <path
          className={styles.path}
          d="m -601.74289,104.20127 -0.41274,-3.32133 m 3.18815,-0.50162 -3.20905,0.5055 m -1.31543,-0.979077 3.91895,2.806337"/>}
        {pawsDisplayed > 4 && <path
          className={styles.path}
          d="m -594.49179,99.621704 -1.53672,-2.973202 m 2.81751,-1.574031 -2.83578,1.584909 m -1.57299,-0.463307 4.64801,1.276567"/>}
      </g>
    </svg>
  );
}
