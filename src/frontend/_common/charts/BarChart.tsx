import React, { ComponentProps } from "react";
import { Bar } from "react-chartjs-2";
import AutoSizer from "react-virtualized-auto-sizer";
import styles from "./_css/barChart.module.css";

type Props = {
  title?: string;
  height?: number;
  options: ComponentProps<typeof Bar>;
};

export function BarChart(props: Props) {
  return (
    <AutoSizer disableHeight>
      {({ width }) => {
        return (
          <div style={{ width }}>
            {props.title && (
              <h4 className="text-lg mb-4 flex flex-row items-center">
                <div className={styles.bullet}/>
                {props.title}
              </h4>
            )}
            <Bar
              width={width}
              height={300}
              {...props.options}
              options={{
                responsive: true,
                scales: {
                  xAxes: [{
                    stacked: true,
                  }],
                  yAxes: [{
                    stacked: true,
                  }],
                },
                ...props.options.options,
              }}
            />
          </div>
        );
      }}
    </AutoSizer>
  );
}
