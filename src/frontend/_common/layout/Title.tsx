import React, { ReactNode } from "react";
import styles from "./_css/title.module.css";
import clsx from "clsx";

type Props = {
  line1: string;
  line2?: string;
  className?: string;
  icon?: ReactNode;
};

export function Title(props: Props) {
  return (
    <h3 className={clsx(styles.container, props.className)}>
      <div>{props.line1}</div>
      {props.line2 && <div>{props.line2}</div>}
      <div className="flex flex-row items-center mt-1">
        <div className={styles.border}/>
        {
          props.icon && (
            <div className={clsx("ml-1", styles.icon)}>
              {props.icon}
            </div>
          )
        }
      </div>
    </h3>
  );
}
