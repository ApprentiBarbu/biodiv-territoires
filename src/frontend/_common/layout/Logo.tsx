import Link from "next/link";
import React from "react";
import styles from "./_css/logo.module.css";
import { URLS } from "../../../shared/_configs/URLS";

export function Logo() {
  return (
    <Link href={URLS.home()}>
      <div className={styles.container}>
        <div>Biodiv'</div>
        <div>Territoires</div>
      </div>
    </Link>
  );
}
