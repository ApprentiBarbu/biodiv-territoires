import React, { useState } from "react";
import { Logo } from "../Logo";
import { URLS } from "../../../../shared/_configs/URLS";
import styles from "./_css/header.module.css";
import clsx from "clsx";
import { HeaderLink } from "./HeaderLink";
import { useRouter } from "next/router";
import { SearchOverlay } from "../../../territories/search/SearchOverlay";
import { BiSearchAlt } from "react-icons/bi";

export function Header() {
  const [searchOpen, setSearchOpen] = useState(false);
  const router = useRouter();

  const isTransparent = router.pathname === URLS.home();

  return (
    <div
      className={clsx("flex flex-row left-0 right-0 top-0 items-center transition-all", clsx(styles.container, { [styles.transparent]: isTransparent }))}
    >
      <div className="mr-20 shouldBlur">
        <Logo/>
      </div>
      <div className="flex flex-row items-center shouldBlur">
        <HeaderLink label="accueil" href={URLS.home()}/>
        <HeaderLink label="à propos" href={URLS.aboutUs()}/>
        <HeaderLink label="contact" href={URLS.contact()}/>
        <HeaderLink icon={<BiSearchAlt size={18}/>} label="rechercher" onClick={() => setSearchOpen(true)}/>
      </div>
      <SearchOverlay open={searchOpen} onClose={() => setSearchOpen(false)}/>
    </div>
  );
}
