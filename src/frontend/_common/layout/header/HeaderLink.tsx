import React, { ReactNode } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import clsx from "clsx";

type Props = {
  href?: string;
  label: string;
  onClick?: () => void;
  icon?: ReactNode;
};

export function HeaderLink(props: Props) {
  const router = useRouter();
  const match = router.pathname === props.href;
  return (
    <div
      className={clsx("font-bold mx-8 opacity-50 transition-all hover:opacity-100 text-lg cursor-pointer flex flex-row items-center", { "opacity-100": match })}
      onClick={props.onClick}
    >
      {props.icon && <div className="mr-1" style={{ transform: "translateY(2px)" }}>{props.icon}</div>}
      {
        props.href
          ? (
            <Link href={props.href}>
              {props.label}
            </Link>
          )
          : props.label
      }
    </div>
  );
}
