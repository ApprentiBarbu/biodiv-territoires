import React from "react";
import clsx from "clsx";
import { Button } from "../buttons/Button";
import { AppError } from "./_models/appError.model";
import { errorUtils } from "./_utils/error.utils";

type Props = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> & {
  error?: AppError | string;
  onRetry?: () => void;
};

export const ErrorBlock = React.forwardRef<HTMLDivElement, Props>(
  ({ error, className, onRetry, ...props }: Props, ref) => {

    if (!error) return <div/>;
    return (
      <div className={clsx("text-red-500", className)} {...props} ref={ref}>
        {errorUtils.getErrorMessage(error)}
        {onRetry ? (
          <div className="mt-2 cursor-pointer underline">
            <Button onClick={onRetry}>Réessayer</Button>
          </div>
        ) : null}
      </div>
    );
  },
);
