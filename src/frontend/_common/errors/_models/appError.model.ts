export type AppError = {
  key: string;
  params?: any;
};
