import React from "react";

type Props = React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>;

export function Button({ className, ...props }: Props) {
  return (
    <div className={className} {...props}/>
  );
}
