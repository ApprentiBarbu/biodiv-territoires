import React from "react";
import { LAYERS } from "./_models/layers.model";
import { Dropdown } from "../forms/dropdown/Dropdown";
import styles from "./_css/layerSelector.module.css";

type Props = {
  selectedLayerId: string;
  onSelect: (layerId: string) => void;
};

export function LayerSelector(props: Props) {
  return (
    <div className={styles.container}>
      <Dropdown
        value={props.selectedLayerId}
        options={LAYERS.map((layer) => ({ value: layer.id, label: layer.title }))}
        onChange={props.onSelect}
      />
    </div>
  );
}
