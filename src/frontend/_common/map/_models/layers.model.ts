import { ComponentProps } from "react";
import { TileLayer, WMSTileLayer } from "react-leaflet";

type BaseLayer = {
  id: string;
  title: string;
  urlInfo?: string;
  wms: boolean;
}

type WmsLayer = BaseLayer & {
  wms: true;
  props: ComponentProps<typeof WMSTileLayer>;
}

type TileLayer = BaseLayer & {
  wms: false;
  props: ComponentProps<typeof TileLayer>;
}

type Layer = WmsLayer | TileLayer;

export const LAYERS: Layer[] = [
  {
    id: "carte",
    title: "Carte",
    wms: false,
    props: {
      attribution: "IGN-F/Geoportail",
      minZoom: 0,
      maxZoom: 18,
      tileSize: 256,
      url: "https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
    },
  },
  {
    id: "ortho",
    title: "Orthophotographie",
    wms: false,
    props: {
      minZoom: 0,
      maxZoom: 18,
      attribution: "IGN-F/Geoportail",
      tileSize: 256,
      url: "https://wxs.ign.fr/pratique/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
    },
  },
  {
    id: "pollum",
    title: "Pollution lumineuse",
    urlInfo: "/pollum",
    wms: true,
    props: {
      layers: "opendata:pollum_aura",
      minZoom: 0,
      maxZoom: 18,
      attribution: "Pollution lumineuse généré d'après les données de <a href=\"https://ngdc.noaa.gov/eog/viirs/download_dnb_composites.html\" target=\"_blank\" data-toggle=\"tooltip\" title=\"Lien vers les données source\"><b>Earth Observation Group, NOAA National Centers for Environmental Information (NCEI)</b></a>",
      tileSize: 256,
      url: "https://data.lpo-aura.org/geoserver/wms?",
    },
  },
  {
    id: "clc",
    title: "Corine Land Cover",
    urlInfo: "/clc",
    wms: true,
    props: {
      layers: "LANDCOVER.CLC18_FR",
      minZoom: 0,
      maxZoom: 18,
      attribution: "Corine Land Cover 2018",
      tileSize: 256,
      url: "http://wxs.ign.fr/corinelandcover/geoportail/r/wms?",
    },
  },
];
