import React, { PropsWithChildren, ReactNode } from "react";
import { LAYERS } from "./_models/layers.model";
import { MapContainer, MapContainerProps, TileLayer, WMSTileLayer } from "react-leaflet";
import { LatLngExpression } from "leaflet";
import { LayerSelector } from "./LayerSelector";

type Props = {
  center: LatLngExpression;
  controlChildren?: ReactNode;
  mapContainerProps?: Partial<MapContainerProps>;
}

const MAP_CONTAINER_STYLE = { height: 500 };

export const BaseMap = React.memo((props: PropsWithChildren<Props>) => {
  const [selectedLayerId, setSelectedLayerId] = React.useState(LAYERS[1].id);
  const layer = LAYERS.find((layer) => layer.id === selectedLayerId);


  return (
    <div className="relative">
      <MapContainer
        center={props.center}
        zoom={11}
        style={MAP_CONTAINER_STYLE}
        {...props.mapContainerProps}
      >
        {
          layer.wms
            ? <WMSTileLayer key={layer.id} {...layer.props}/>
            : <TileLayer key={layer.id} {...layer.props}/>
        }
        {props.children}
      </MapContainer>
      <LayerSelector selectedLayerId={selectedLayerId} onSelect={setSelectedLayerId}/>
      {props.controlChildren}
    </div>
  );
});
