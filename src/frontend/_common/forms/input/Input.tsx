import React, { ReactNode } from "react";
import styles from "./_css/input.module.css";
import clsx from "clsx";

export enum INPUT_THEME {
  WHITE, BLACK
}

export enum INPUT_SIZE {
  BIG, EXTRA_BIG
}

type Props = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
  onTextChange?: (value: string) => void;
  iconRenderer?: (width: number, height: number) => ReactNode;
  theme?: INPUT_THEME;
  size?: INPUT_SIZE;
};

const ICON_SIZES: Record<INPUT_SIZE, [number, number]> = {
  [INPUT_SIZE.BIG]: [25, 25],
  [INPUT_SIZE.EXTRA_BIG]: [28, 28],
};

export const Input = React.memo((props: Props) => {
  let { onTextChange, iconRenderer, value, onChange, className, theme, size, style, ...inputProps } = props;
  theme = theme ?? INPUT_THEME.WHITE;
  size = size ?? INPUT_SIZE.BIG;
  const iconSize = ICON_SIZES[size];
  return (
    <div className="relative">
      <input
        className={clsx(styles.container, {
          [styles.white]: theme === INPUT_THEME.WHITE,
          [styles.black]: theme === INPUT_THEME.BLACK,
          [styles.big]: size === INPUT_SIZE.BIG,
          [styles.extraBig]: size === INPUT_SIZE.EXTRA_BIG,
        }, className)}
        value={value ?? ""}
        onChange={(event) => {
          onChange?.(event);
          onTextChange?.(event.target.value);
        }}
        style={{ ...style, paddingLeft: iconRenderer ? iconSize[0] + 5 : 0 }}
        {...inputProps}
      />
      {iconRenderer && <div className={styles.icon}>{iconRenderer(iconSize[0], iconSize[1])}</div>}
    </div>
  );
});
