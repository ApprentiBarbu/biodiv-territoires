import React, { useState } from "react";
import { PopoverContainer } from "../../popover/PopoverContainer";
import { Popover } from "../../popover/Popover";
import styles from "./_css/dropdown.module.css";
import clsx from "clsx";
import { RiArrowDownSLine } from "react-icons/ri";

type Props<TValue> = {
  value?: TValue;
  options: { value: TValue; label: string }[]
  placeholder?: string;
  onChange: (value: TValue) => void;
  width?: number;
  theme?: "inline" | "default";
};

export function Dropdown<TValue extends (string | number)>(props: Props<TValue>) {
  const [popoverOpened, setPopoverOpened] = useState(false);

  const isInline = props.theme === "inline";
  const width = props.width ?? (isInline ? undefined : 200);
  const selectedOption = props.options.find((option) => option.value === props.value);

  return (
    <PopoverContainer>
      <div
        className={clsx("transition-all flex flex-row items-center select-none", styles.container, {
          [styles.container_inline]: isInline,
          [styles.container_opened]: popoverOpened,
        })}
        onClick={() => setPopoverOpened(!popoverOpened)}
        style={{ width }}
      >
        <div className="flex-1">
          {selectedOption?.label ?? props.placeholder ?? "-"}
        </div>
        <div className={clsx("transition-all", styles.arrow)}>
          <RiArrowDownSLine size={18}/>
        </div>
      </div>
      <Popover
        open={popoverOpened}
        onClose={() => setPopoverOpened(false)}
        className={styles.popoverContent}
        style={{ width }}
      >
        {
          props.options.map((option) => (
            <div
              key={option.value}
              className={clsx(styles.option, { [styles.option_selected]: option === selectedOption })}
              onClick={() => {
                props.onChange(option.value);
                setPopoverOpened(false);
              }}
            >
              {option.label}
            </div>
          ))
        }
      </Popover>
    </PopoverContainer>
  );
}
