import { useEffect, useRef, useState } from "react";

export function useAutocomplete<T>(getSuggestions: (value: string) => Promise<T[]> | T[], value: string) {
  const valueRef = useRef(value);

  const [loading, setLoading] = useState(false);
  const [suggestions, setSuggestions] = useState<T[]>([]);

  useEffect(() => {
    valueRef.current = value;
    const query = getSuggestions(value);

    if ("then" in query) {
      setLoading(true);
      query.then((result) => {
        if (value === valueRef.current) {
          setSuggestions(result);
          setLoading(false);
        }
      });
    } else {
      setSuggestions(query);
      setLoading(false);
    }
  }, [getSuggestions, value]);

  return { loading, suggestions };
}