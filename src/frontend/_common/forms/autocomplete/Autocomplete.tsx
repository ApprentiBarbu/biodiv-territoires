import React, { ReactNode, useEffect, useState } from "react";
import { PopoverContainer } from "../../popover/PopoverContainer";
import { Popover } from "../../popover/Popover";
import styles from "./_css/autocomplete.module.css";
import { Spinner } from "../../loadings/Spinner";
import { useAutocomplete } from "./useAutocomplete";

type Props<T> = {
  value: string;
  className?: string;
  getSuggestions: (value: string) => Promise<T[]> | T[];
  renderSuggestions: (suggestions: T[]) => ReactNode;
  children: (onOpen: () => void) => ReactNode;
};

export function Autocomplete<T>(props: Props<T>) {
  const [popoverOpened, setPopoverOpened] = useState(false);
  const { loading, suggestions } = useAutocomplete(props.getSuggestions, props.value);

  useEffect(() => {
    if (props.value.trim().length > 0) {
      setPopoverOpened(true);
    }
  }, [props.value]);

  return (
    <PopoverContainer className={props.className}>
      {props.children(() => setPopoverOpened(true))}
      <Popover
        open={popoverOpened}
        onClose={() => setPopoverOpened(false)}
      >
        <div className={styles.popoverContent}>
          {
            loading
              ? (<div className={styles.loading}><Spinner/></div>)
              : props.renderSuggestions(suggestions)
          }
        </div>
      </Popover>
    </PopoverContainer>
  );
}
