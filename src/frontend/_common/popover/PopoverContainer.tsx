import React, { PropsWithChildren } from "react";
import clsx from "clsx";

type Props = {
  className?: string;
  inline?: boolean;
};

export function PopoverContainer(props: PropsWithChildren<Props>) {
  return (
    <div
      className={clsx({ "inline-block": props.inline }, "relative", props.className)}
      onClick={(event) => event.stopPropagation()}
    >
      {props.children}
    </div>
  );
}
