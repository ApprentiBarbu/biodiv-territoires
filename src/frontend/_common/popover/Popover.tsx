import React, { CSSProperties, PropsWithChildren, useEffect, useMemo, useRef, useState } from "react";
import clsx from "clsx";
import { AnimatePresence, motion } from "framer-motion";

type Props = {
  className?: string;
  style?: CSSProperties;
  open: boolean;
  onClose: () => void;
};

const popoverVariants = {
  open: { opacity: 1, y: 0 },
  hidden: { opacity: 0, y: -300 },
};

export function Popover(props: PropsWithChildren<Props>) {
  const [recomputeHeightCounter, setRecomputeHeightCounter] = useState(0);
  const containerRef = useRef<HTMLDivElement>();

  let maxHeight = useMemo(() => {
    if (containerRef.current) {
      const boundingClientRect = containerRef.current.getBoundingClientRect();
      return window.innerHeight - boundingClientRect.top - 40;
    }
    return 300;
  }, [props.open, recomputeHeightCounter]);

  useEffect(() => {
    if (props.open) {
      const resizeListener = () => {
        setRecomputeHeightCounter(recomputeHeightCounter + 1);
      };
      window.addEventListener("resize", resizeListener);
      window.addEventListener("click", props.onClose);
      return () => {
        window.removeEventListener("resize", resizeListener);
        window.removeEventListener("click", props.onClose);
      };
    }
  }, [props.open, props.onClose, recomputeHeightCounter, setRecomputeHeightCounter]);

  const displayAbove = maxHeight < 100;
  maxHeight = displayAbove ? 300 : maxHeight;

  return (
    <div
      ref={(ref) => {
        containerRef.current = ref;
      }}
      className={clsx("absolute overflow-auto rounded-b-sm left-0 right-0 z-10", props.className)}
      style={{ ...props.style, top: displayAbove ? undefined : 40, bottom: displayAbove ? 35 : undefined }}
    >
      <AnimatePresence>
        {props.open && (
          <motion.div
            initial={{ opacity: 0, maxHeight: 0 }}
            animate={{ opacity: 1, maxHeight }}
            exit={{ opacity: 0, maxHeight: 0 }}
          >
            {props.children}
          </motion.div>
        )}
      </AnimatePresence>
    </div>
  );
}
