import React from "react";
import { BirdPawsLoader } from "../paws/BirdPawsLoader";

export function Spinner() {
  return (
    <div className="flex flex-col items-center">
      <div className="mb-1">
        <BirdPawsLoader animate/>
      </div>
      <div>chargement en cours</div>
    </div>
  );
}
