import React from "react";
import ContentLoader from "react-content-loader";

type Props = {
  width?: number;
  height?: number;
}

export function Skeleton(props: Props) {
  const height = props.height ?? 36;
  return (
    <ContentLoader height={height} viewBox={`0 0 ${props.width ?? 100} ${height}`}>
      <rect x="0" y="0" rx="3" ry="3" width={props.width} height={props.height}/>
    </ContentLoader>
  );
}
