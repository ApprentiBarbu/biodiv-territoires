import React, { PropsWithChildren, ReactNode } from "react";
import { Spinner } from "./Spinner";
import { ErrorBlock } from "../errors/ErrorBlock";
import { AppError } from "../errors/_models/appError.model";
import { motion } from "framer-motion";

type Props = {
  isLoading: boolean;
  error?: AppError;
  onRetry?: () => void;
  spinner?: ReactNode;
};

export function Loader(props: PropsWithChildren<Props>) {
  if (props.isLoading) return <>{props.spinner ?? <Spinner/>}</>;
  if (props.error) return <ErrorBlock error={props.error} onRetry={props.onRetry}/>;
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} transition={{ duration: 1 }}>
      {props.children}
    </motion.div>
  );
}
