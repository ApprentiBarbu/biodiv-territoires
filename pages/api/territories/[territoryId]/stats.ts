import type { NextApiRequest, NextApiResponse } from "next";
import { TerritoryStatsResult } from "../../../../src/shared/territories/_models/territoryStats.model";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoryStatsResult>) {
  if (req.method === "GET") {
    const result: TerritoryStatsResult = {
      observations: 2455,
      observers: 24,
      observedDates: 201,
      lastObservation: new Date().getTime(),
      threatenedSpecies: 6,
      species: 104,
    };
    res.status(200).json(result);
  }
}
