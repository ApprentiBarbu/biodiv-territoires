import type { NextApiRequest, NextApiResponse } from "next";
import { TerritoryThreatenedResult } from "../../../../src/shared/territories/_models/territoryThreatened.model";
import { fetchUtils } from "../../../../src/shared/_utils/Fetch.utils";
import { apiConfig } from "../../../../src/api/_configs/api.configs";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoryThreatenedResult>) {
  if (req.method === "GET") {
    const { query: { territoryId } } = req;
    const range = req.query.range;
    const result = await fetchUtils.get<TerritoryThreatenedResult>(`${apiConfig.sourceApiUrl}/charts/synthesis/group2_inpn_species/${territoryId}/${range}`);
    res.status(200).json(result.data);
  }
}
