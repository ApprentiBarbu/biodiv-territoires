import type { NextApiRequest, NextApiResponse } from "next";
import { fetchUtils } from "../../../../src/shared/_utils/Fetch.utils";
import { apiConfig } from "../../../../src/api/_configs/api.configs";
import { TerritoryTilesResult } from "../../../../src/shared/territories/_models/territoryTiles.model";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoryTilesResult>) {
  if (req.method === "GET") {
    const { query: { territoryId } } = req;
    const range = req.query.range;
    const result = await fetchUtils.get<TerritoryTilesResult>(`${apiConfig.sourceApiUrl}/grid_data/${territoryId}/${range}/M0.5`);
    res.status(200).json(result.data);
  }
}
//https://biodiv-territoires.lpo-aura.org/api/list_taxa/simp/2041198?_=1614942909108