import { fetchUtils } from "../../../src/shared/_utils/Fetch.utils";
import { apiConfig } from "../../../src/api/_configs/api.configs";
import type { NextApiRequest, NextApiResponse } from "next";
import { TerritoryGetResult } from "../../../src/shared/territories/_models/territoryGet.model";
import { TerritoriesSearchResult } from "../../../src/shared/territories/_models/territoriesSearch.model";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoryGetResult>) {
  if (req.method === "GET") {
    const { query: { territoryId } } = req;
    // TODO change the api to return the id
    // const result = await fetchUtils.get<TerritoryGetResult>(`${apiConfig.sourceApiUrl}/com/${territoryId}`);
    const result = await fetchUtils.get<TerritoriesSearchResult>(`${apiConfig.sourceApiUrl}/find/area?q=${territoryId}`);
    res.status(200).json(result.data.datas[0]);
  }
}
