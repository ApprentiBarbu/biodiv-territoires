import { fetchUtils } from "../../../src/shared/_utils/Fetch.utils";
import { apiConfig } from "../../../src/api/_configs/api.configs";
import type { NextApiRequest, NextApiResponse } from "next";
import { TerritoriesConfResult } from "../../../src/shared/territories/_models/territoriesConf.model";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoriesConfResult>) {
  if (req.method === "GET") {
    const result = await fetchUtils.get<TerritoriesConfResult>(`${apiConfig.sourceApiUrl}/territory/conf/ntile/`);
    res.status(200).json(result.data);
  }
}
