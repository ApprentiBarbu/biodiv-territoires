import { fetchUtils } from "../../../src/shared/_utils/Fetch.utils";
import { apiConfig } from "../../../src/api/_configs/api.configs";
import { TerritoriesSearchResult } from "../../../src/shared/territories/_models/territoriesSearch.model";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoriesSearchResult>) {
  if (req.method === "POST") {
    const searchedText = req.query.text;
    const result = await fetchUtils.get<TerritoriesSearchResult>(`${apiConfig.sourceApiUrl}/find/area?q=${searchedText}`);
    res.status(200).json(result.data);
  }
}
