import { fetchUtils } from "../../../src/shared/_utils/Fetch.utils";
import { apiConfig } from "../../../src/api/_configs/api.configs";
import type { NextApiRequest, NextApiResponse } from "next";
import { TerritoryTileSpeciesResult } from "../../../src/shared/territories/_models/territoryTileSpecies.model";

export default async function handler(req: NextApiRequest, res: NextApiResponse<TerritoryTileSpeciesResult>) {
  if (req.method === "GET") {
    const { query: { tileId } } = req;
    const result = await fetchUtils.get<TerritoryTileSpeciesResult>(`${apiConfig.sourceApiUrl}/list_taxa/simp/${tileId}`);
    res.status(200).json(result.data);
  }
}
