import React from "react";
import { useRouter } from "next/router";
import { TerritoryScreen } from "../../src/frontend/territories/screen/TerritoryScreen";
import { territoriesUtils } from "../../src/frontend/territories/_utils/Territories.utils";

type Props = {};

export default function TerritoryPage(props: Props) {
  const router = useRouter();
  const { territorySlug } = router.query;

  if (typeof territorySlug !== "string") return null; // TODO 404
  
  return (
    <TerritoryScreen territoryCode={territoriesUtils.extractCodeFromSlug(territorySlug)}/>
  );
}
