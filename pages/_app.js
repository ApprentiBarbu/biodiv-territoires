import 'tailwindcss/tailwind.css';
import './_css/global.css';
import './_css/reactAutosuggest.css';
import Head from 'next/head';
import {Header} from '../src/frontend/_common/layout/header/Header';

function MyApp({Component, pageProps}) {
  return (
    <>
      <Head>
        <title>Biodiv'Territoires</title>
      </Head>
      <Header/>
      <div className="shouldBlur">
        <Component {...pageProps} />
      </div>
    </>
  );
}

export default MyApp;
